# DIVOC
Open source digital platform for large scale vaccination and certification programs. Built for India scale.

> __[Latest changes are on orchestration branch](https://github.com/egovernments/DIVOC/tree/orchestration)__

## [Documentation](https://divoc.egov.org.in)

## [Discuss with community](https://github.com/egovernments/DIVOC/discussions)

## [Issue Reporting](https://github.com/egovernments/DIVOC/issues)

